import React from "react";
import { HomeScreen, LoginScreen, Camera, Analytic } from '../screen'
import { createNativeStackNavigator } from "@react-navigation/native-stack"
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

const Stack = createNativeStackNavigator()
const Tab = createBottomTabNavigator();


const MainApp = () => {
    return (

        <Tab.Navigator>
            <Tab.Screen name="Home" component={HomeScreen} options={{ headerShown: false }} />
            <Tab.Screen name="Camera" component={Camera} />
            <Tab.Screen name="Analytic" component={Analytic} />
        </Tab.Navigator>


    )
}

const Router = () => {
    return (

        <Stack.Navigator initialRouteName="Login">
            <Stack.Screen name="MainApp" component={MainApp} options={{ headerShown: false }} />
            <Stack.Screen name="Login" component={LoginScreen} options={{ headerShown: false }} />
        </Stack.Navigator>


    )
}

export default Router;