import HomeScreen from './HomeScreen';
import LoginScreen from './LoginScreen';
import Camera from './CameraScreen';
import WebScreen from './WebView';
import Analytic from './Analyctic';

export {HomeScreen, LoginScreen, Camera, WebScreen, Analytic}