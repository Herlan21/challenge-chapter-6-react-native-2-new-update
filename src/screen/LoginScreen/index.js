import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Button } from 'react-native';
import auth from '@react-native-firebase/auth';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import Biometric from '../Biometric';

const LoginScreen = ({ navigation }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  // Set an initializing state whilst Firebase connects
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();

  function onLogin() {
    auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        console.log('User account created & signed in!');
        // navigation.navigate('MainApp')
      })
      .catch(error => {
        if (error.code === 'auth/email-already-in-use') {
          console.log('That email address is already in use!');
        }

        if (error.code === 'auth/invalid-email') {
          console.log('That email address is invalid!');
        }

        console.error(error);
      });
  }

  function onLogout() {
    auth()
      .signOut()
      .then(() => console.log('User signed out!'));
  }

  function googleSignin() {
    GoogleSignin.configure({
      webClientId: '36843269248-b9vraa3qegpa9p648ipvoctfu6ij5teo.apps.googleusercontent.com',
    });
  }
  async function onGoogleButtonPress() {
    // Get the users ID token
    const { idToken } = await GoogleSignin.signIn();

    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);

    // Sign-in the user with the credential
    return auth().signInWithCredential(googleCredential);
  }

  // Handle user state changes
  function onAuthStateChanged(user) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    googleSignin()
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if (initializing) return null;

  if (!user) {
    return (
      <View style={styles.container}>

        <View style={styles.wrapper}>
          <Text style={styles.title}>Sign In to your account</Text>

          <View>

            <Text style={{ fontSize: 14, color: '#000', fontWeight: '600', marginBottom: 8 }}>Email</Text>
            <TextInput
              style={styles.textinput}
              placeholder="Email"
              value={email}
              onChangeText={value => setEmail(value)}
              placeholderTextColor="#bbb"

            />

            <Text style={{ fontSize: 14, color: '#000', fontWeight: '600', marginBottom: 8 }}>Password</Text>
            <TextInput
              style={styles.textinput}
              placeholderTextColor="#bbb"
              placeholder="Password"
              value={password}
              secureTextEntry={true}
              onChangeText={value => setPassword(value)}

            />

            <View style={styles.LoginButton}>
              <TouchableOpacity onPress={onLogin}>
                <Text style={styles.login}>Login</Text>
              </TouchableOpacity>

              <Button
                title="Google Sign-In"
                onPress={() => onGoogleButtonPress().then(() => console.log('Signed in with Google!'))}
              />
            </View>
          </View>
        </View>
      </View>
    )
  }

  return (
    <View>
      <Biometric />
    </View>

  );

}


export default LoginScreen

const styles = StyleSheet.create({
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#8591d6',
    margin: 10,
    textAlign: 'center',
    top: -20
  },

  wrapper: {
    width: '80%'
  },

  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },

  register: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'center'
  },

  textinput: {
    borderWidth: 0.95,
    borderColor: '#bbb',
    marginBottom: 12,
    borderRadius: 8,
    color: '#000',
    paddingHorizontal: 15
  },
  login: {
    color: '#FFF',
    backgroundColor: '#8591d6',
    textAlign: 'center',
    width: 100,
    height: 35,
    borderRadius: 8,
    fontSize: 21,
    fontWeight: 'bold'
  },

  LoginButton: {
    alignItems: 'center',
    top: 5
  }
})