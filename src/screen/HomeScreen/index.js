import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { Dimensions } from 'react-native';
import MapView, { Marker, Callout } from 'react-native-maps';


const HomeScreen = () => {
  return (
    <View style={styles.container}>
      <MapView style={styles.map}
        initialRegion={{
          latitude: -6.869809706637371,
          longitude: 107.55448070268832,
          latitudeDelta: 0.009,
          longitudeDelta: 0.009,
        }}>

        <Marker
          coordinate={{
            latitude: -6.869809706637371,
            longitude: 107.55448070268832
          }}
          title="Test Title"
          description="This is the test description">

          <Callout tooltip>
            <View>
              <View style={styles.buble}>
                <Text style={styles.name}>Gedug Pemerintahan Kota Cimahi</Text>
                <View style={styles.arrowBorder} />
                <View style={styles.arrow} />
              </View>
              <View style={styles.arrowBorder}></View>
              <View style={styles.arrow}></View>
            </View>
          </Callout>
        </Marker>

        <Marker
          coordinate={{
            latitude: -6.867724132446936,
            longitude: 107.55271755763022 
          }}
          title="Test Title"
          description="This is the test description">

          <Callout tooltip>
            <View>
              <View style={styles.buble}>
                <Text style={styles.name}>Kolam Renang Ciawitali</Text>
                <View style={styles.arrowBorder} />
                <View style={styles.arrow} />
              </View>
              <View style={styles.arrowBorder}></View>
              <View style={styles.arrow}></View>
            </View>
          </Callout>
        </Marker>

        <Marker
          coordinate={{
            latitude: -6.869255000442706,
            longitude: 107.55120247908185 
          }}
          title="Test Title"
          description="This is the test description">

          <Callout tooltip>
            <View>
              <View style={styles.buble}>
                <Text style={styles.name}>Cafe</Text>
                <View style={styles.arrowBorder} />
                <View style={styles.arrow} />
              </View>
              <View style={styles.arrowBorder}></View>
              <View style={styles.arrow}></View>
            </View>
          </Callout>
        </Marker>
        <Marker
          coordinate={{
            latitude: -6.857467,
            longitude: 107.559247
          }}
          title="Test Title"
          description="This is the test description">

          <Callout tooltip>
            <View>
              <View style={styles.buble}>
                <Text style={styles.name}>Home</Text>
                <View style={styles.arrowBorder} />
                <View style={styles.arrow} />
              </View>
              <View style={styles.arrowBorder}></View>
              <View style={styles.arrow}></View>
            </View>
          </Callout>
        </Marker>

      </MapView>
    </View>
  )
}

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default HomeScreen

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
  },

  map: {
    ...StyleSheet.absoluteFillObject,
  }
})